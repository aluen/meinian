package com.atguigu.test;

import com.atguigu.util.SMSUtils;
import com.atguigu.util.ValidateCodeUtils;
import org.junit.Test;

/**
 * @author captainlhy
 * @create 2021-06-05 20:16
 */
public class TestSms {
    /**
     * 短信测试
     */
    @Test
    public void test() throws Exception {
        Integer code = ValidateCodeUtils.generateValidateCode(4);
        SMSUtils.sendShortMessage("15836815266",String.valueOf(code));

    }
}
