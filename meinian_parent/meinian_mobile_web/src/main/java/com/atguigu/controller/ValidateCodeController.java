package com.atguigu.controller;

import com.atguigu.constant.MessageConstant;
import com.atguigu.entity.Result;
import com.atguigu.util.SMSUtils;
import com.atguigu.util.ValidateCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

/**
 * @author captainlhy
 * @create 2021-06-06 11:06
 */
@RestController
@RequestMapping("/validateCode")
public class ValidateCodeController {
    @Autowired
    JedisPool jedisPool;
    @RequestMapping("/send4Order")
    public Result send4Order(String telephone) {
        try {
            //1.生成校验码
//            Integer code = ValidateCodeUtils.generateValidateCode(4);
            //2.发送手机校验码
//            SMSUtils.sendShortMessage(telephone,code.toString());

            //3.将校验码存储到Redis中，用于后面验证码校验
            //jedisPool.getResource().setex(telephone+RedisMessageConstant.SENDTYPE_ORDER,5*60,code.toString());

            return new Result(true, MessageConstant.SEND_VALIDATECODE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.SEND_VALIDATECODE_FAIL);
        }

    }

    @RequestMapping("/send4Login")
    public Result send4Login(String telephone) {
        try {
            //1.生成校验码
//            Integer code = ValidateCodeUtils.generateValidateCode(4);
            //2.发送手机校验码
//            SMSUtils.sendShortMessage(telephone,code.toString());

            //3.将校验码存储到Redis中，用于后面验证码校验
            //jedisPool.getResource().setex(telephone+RedisMessageConstant.SENDTYPE_ORDER,5*60,code.toString());

            return new Result(true, MessageConstant.SEND_VALIDATECODE_SUCCESS);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.SEND_VALIDATECODE_FAIL);
        }

    }
}
