package com.atguigu.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.atguigu.constant.MessageConstant;
import com.atguigu.constant.RedisMessageConstant;
import com.atguigu.entity.Result;
import com.atguigu.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.JedisPool;

import java.util.Map;

/**
 * @author captainlhy
 * @create 2021-06-07 13:51
 */
@RestController
@RequestMapping("/order")
public class OrderMobileController {
    @Reference
    OrderService orderService;

    @Autowired
    JedisPool jedisPool;
    @RequestMapping("/save")
    public Result save(@RequestBody Map map){
        //获取验证码检验验证码是否正确
        try {
            String validateCode = (String)map.get("validateCode");
            String telephone = (String)map.get("telephone");
            String redisCode = jedisPool.getResource().get(telephone+ RedisMessageConstant.SENDTYPE_ORDER);

            if(redisCode==null || !redisCode.equals(validateCode)){
                return new Result(false, MessageConstant.VALIDATECODE_ERROR);
            }
            //jedisPool.getResource().del(telephone+RedisMessageConstant.SENDTYPE_ORDER);
            //保存订单
            Result result = orderService.save(map);
            //发送预约成功短信，略。。。
            return result ;
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.ORDER_FAIL);
        }
    }
    @RequestMapping("/findById")
    public Result findById(Integer orderId){
        try {
            Map map = orderService.findById(orderId);
            return new Result(true,MessageConstant.QUERY_ORDER_SUCCESS,map);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_ORDER_FAIL);
        }
    }
}
