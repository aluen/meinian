package com.atguigu.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.atguigu.entity.PageResult;
import com.atguigu.entity.QueryPageBean;
import com.atguigu.pojo.Address;
import com.atguigu.service.AddressService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author captainlhy
 * @create 2021-06-08 20:49
 */
@RestController
@RequestMapping("/address")
public class AddressController {

    @Reference
    AddressService addressService;

    @RequestMapping("/findAllMaps")
    public Map findAllMaps() {
        Map data = new HashMap();
        List<Address> listAll = addressService.findAll();
        List<Map<String, Object>> gridns = new ArrayList<>();
        List<Map<String, String>> names = new ArrayList<>();
        for (Address address : listAll) {
            Map nameMap = new HashMap();
            nameMap.put("addressName",address.getAddressName());
            names.add(nameMap);

            Map gridnMap = new HashMap();
            gridnMap.put("lng",address.getLng());
            gridnMap.put("lat",address.getLat());
            gridns.add(gridnMap);
        }

        data.put("gridnMaps", gridns);
        data.put("nameMaps", names);
        return data;
    }

    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult result = addressService.findPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize(), queryPageBean.getQueryString());
        return result;
    }
}
