package com.atguigu.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.atguigu.dao.OrderSettingDao;
import com.atguigu.pojo.OrderSetting;
import com.atguigu.service.OrderSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author captainlhy
 * @create 2021-06-04 13:53
 */
@Service(interfaceClass = OrderSettingService.class)
@Transactional
public class OrderSettingServiceImpl implements OrderSettingService {
    @Autowired
    OrderSettingDao orderSettingDao;

    @Override
    public void add(List<OrderSetting> orderSettings) {

        //遍历List<OrderSetting> list
        for (OrderSetting orderSetting : orderSettings) {
            //判断当前的日期之前是否设置过
            Date orderDate = orderSetting.getOrderDate();
            int count = orderSettingDao.findCountByOrderDate(orderDate);
            if (count > 0) {
                //设置过, 更新数量
                orderSettingDao.editNumberByOrderDate(orderSetting.getNumber());
            } else {
                //没有设置过, 保存
                orderSettingDao.add(orderSetting);
            }

        }
    }

    @Override
    public List<Map<String, Integer>> getOrderSettingByMonth(String date) {
        String start = date+"-01";  //  2021-6-01
        String end = date+"-31";

        Map map = new HashMap();
        map.put("start",start);
        map.put("end",end);

        List<OrderSetting> list = orderSettingDao.getOrderSettingByMonth(map);

        List<Map<String, Integer>> listData = new ArrayList<Map<String, Integer>>();

        for (OrderSetting orderSetting : list) {
            Map<String, Integer> mapData = new HashMap<String, Integer>();
            mapData.put("date",orderSetting.getOrderDate().getDate());
            mapData.put("number",orderSetting.getNumber());
            mapData.put("reservations",orderSetting.getReservations());
            listData.add(mapData);
        }
        return listData;
    }

    @Override
    public void edit(OrderSetting orderSetting) {
        int count = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
        if(count>0){
            orderSettingDao.edit(orderSetting);
        }else{
            orderSettingDao.add(orderSetting);
        }
    }
}
