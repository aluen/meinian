package com.atguigu.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.atguigu.constant.MessageConstant;
import com.atguigu.dao.OrderDao;
import com.atguigu.dao.OrderSettingDao;
import com.atguigu.entity.Result;
import com.atguigu.pojo.Member;
import com.atguigu.pojo.Order;
import com.atguigu.pojo.OrderSetting;
import com.atguigu.dao.MemberDao;
import com.atguigu.service.OrderService;
import com.atguigu.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

/**
 * @author captainlhy
 * @create 2021-06-07 14:06
 */
@Service(interfaceClass = OrderService.class)
@Transactional
public class OrderServiceImpl implements OrderService {
    @Autowired
    OrderSettingDao orderSettingDao;

    @Autowired
    MemberDao memberDao;

    @Autowired
    OrderDao orderDao;

    @Override
    public Result save(Map map) throws Exception {
        String orderDateStr = (String) map.get("orderDate");
        Date orderDate = DateUtils.parseString2Date(orderDateStr);

        //1.根据日期查询预约设置，如果不存在那么预约失败
        OrderSetting orderSetting = orderSettingDao.getOrderSettingByOrderDate(orderDate);
        //2.根据日期查询预约设置，如果存在，已预约人数大于等于可预约人数，预约已满
        if (orderSetting == null) {
            return new Result(false, MessageConstant.ORDER_FAIL);
        }
        //3.根据手机号查询会员，如果会员不存在，快速注册；
        String telephone = (String) map.get("telephone");
        Integer setmealId = Integer.parseInt((String) map.get("setmealId"));
        Member member = memberDao.getMemberByTelephone(telephone);
        if (member == null) {
            member = new Member();
            member.setName((String) map.get("name"));
            member.setSex((String) map.get("sex"));
            member.setIdCard((String) map.get("idCard"));
            member.setPhoneNumber(telephone);
            member.setRegTime(new Date());
            memberDao.add(member); //主键回填
        } else {//4.会员存在，根据  会员id,预约日期，套餐id   查询预约订单，如果存在，不能重复预约
            Order order = new Order();
            order.setMemberId(member.getId());
            order.setOrderDate(orderDate);
            order.setSetmealId(setmealId);
            int count = orderDao.getCountByCondition(order);
            if (count > 0) { //说明已经约过了
                return new Result(false, MessageConstant.HAS_ORDERED);
            }
        }
        //5.会员存在，没有预约过，可以预约  reservations++
        orderSetting.setReservations(orderSetting.getReservations() + 1);
        orderSettingDao.editReservationsByOrderDate(orderSetting);

        //6.往订单表保存数据
        Order order = new Order();
        order.setMemberId(member.getId());
        order.setOrderDate(orderDate);
        order.setOrderType(Order.ORDERTYPE_WEIXIN);
        order.setOrderStatus(Order.ORDERSTATUS_NO);
        order.setSetmealId(setmealId);
        orderDao.add(order); //主键回填
        return new Result(true, MessageConstant.ORDER_SUCCESS, order);


    }

    @Override
    public Map findById(Integer orderId) throws Exception {
        Map map = orderDao.findById(orderId);
        Date orderDate = (Date)map.get("orderDate");
        map.put("orderDate", DateUtils.parseDate2String(orderDate));
        return map;
    }
}
