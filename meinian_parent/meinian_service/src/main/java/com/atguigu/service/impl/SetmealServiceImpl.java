package com.atguigu.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.atguigu.constant.RedisConstant;
import com.atguigu.dao.SetmealDao;
import com.atguigu.entity.PageResult;
import com.atguigu.pojo.Setmeal;
import com.atguigu.service.SetmealService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import redis.clients.jedis.JedisPool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author captainlhy
 * @create 2021-06-02 18:03
 */
@Service(interfaceClass = SetmealService.class)
@Transactional
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    SetmealDao setmealDao;
    @Autowired
    JedisPool jedisPool;

    @Override
    public void add(Integer[] travelgroupIds, Setmeal setmeal) {
        setmealDao.add(setmeal);
        if (travelgroupIds != null && travelgroupIds.length > 0) {
            setSetmealAndTravelGroup(setmeal.getId(), travelgroupIds);
        }
        //将图片名称保存到Redis
        savePic2Redis(setmeal.getImg());
    }

    @Override
    public PageResult findPage(Integer currentPage, Integer pageSize, String queryString) {
        PageHelper.startPage(currentPage,pageSize);
        Page<Setmeal> page = setmealDao.findPage(queryString);
        return new PageResult(page.getTotal(),page.getResult());

    }

    @Override
    public Setmeal get(Integer id) {
        return setmealDao.get(id);
    }

    @Override
    public void edit(Integer[] travelgroupIds, Setmeal setmeal) {
        setmealDao.edit(setmeal);
        //删除中间表中的关联数据
        setmealDao.deleteMidData(setmeal.getId());
        //重新添加
        setSetmealAndTravelGroup(setmeal.getId(),travelgroupIds);

    }

    @Override
    public List<Setmeal> findAll() {
        return setmealDao.findAll();
    }

    @Override
    public Setmeal findById(Integer id) {
        return setmealDao.findById(id);
    }

    @Override
    public List<Map<String, Object>> getSetmealReport() {
        return setmealDao.getSetmealReport();
    }

    @Override
    public void delete(Integer id) {
        int count = setmealDao.countTravelGroupIds(id);
        if (count > 0) {
            throw new RuntimeException("存在关联数据,无法删除");
        }
        setmealDao.delete(id);

    }

    @Override
    public List<Integer> getTravelGroupIdsBySetmealId(Integer setmealId) {
        return setmealDao.getTravelGroupIdsBySetmealId(setmealId);
    }

    //将图片名称保存到Redis
    private void savePic2Redis(String pic) {
        jedisPool.getResource().sadd(RedisConstant.SETMEAL_PIC_DB_RESOURCES, pic);
    }

    private void setSetmealAndTravelGroup(Integer id, Integer[] travelgroupIds) {
        for (Integer travelgroupId : travelgroupIds) {
            Map<String, Integer> map = new HashMap<>();
            map.put("travelgroup_id", travelgroupId);
            map.put("setmeal_id", id);
            setmealDao.setSetmealAndTravelGroup(map);

        }
    }
}
