package com.atguigu.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.atguigu.dao.TravelItemDao;
import com.atguigu.entity.PageResult;
import com.atguigu.pojo.TravelItem;
import com.atguigu.service.TravelItemService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author captainlhy
 * @create 2021-05-31 18:48
 */
@Service(interfaceClass = TravelItemService.class)
@Transactional
public class TravelItemServiceImpl implements TravelItemService {

    @Autowired
    TravelItemDao travelItemDao;
    @Override
    public void addItem(TravelItem travelItem) {
        travelItemDao.addItem(travelItem);
    }

    @Override
    public PageResult findPage(Integer currentPage, Integer pageSize, String queryString) {
        PageHelper.startPage(currentPage,pageSize);
        Page <TravelItem> page = travelItemDao.findPage(queryString);
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public TravelItem find(Integer id) {
        return travelItemDao.find(id);
    }

    @Override
    public void edit(TravelItem travelItem) {
        travelItemDao.edit(travelItem);
    }

    @Override
    public void delete(Integer id) {
        int count = travelItemDao.countTravelItemIds(id);//根据自由行id查询中间表
        if (count > 0) {
            throw new RuntimeException("存在关联数据,无法删除");
        }
        travelItemDao.delete(id);

    }

    @Override
    public List<TravelItem> findAll() {
        return travelItemDao.findAll();
    }

}
