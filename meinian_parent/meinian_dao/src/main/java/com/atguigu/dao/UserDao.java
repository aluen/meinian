package com.atguigu.dao;

import com.atguigu.pojo.User;

/**
 * @author captainlhy
 * @create 2021-06-07 21:05
 */
public interface UserDao {
    User findUserByUsername(String username);
}
