package com.atguigu.dao;

import com.atguigu.pojo.TravelItem;
import com.github.pagehelper.Page;

import java.util.List;

/**
 * @author captainlhy
 * @create 2021-05-31 18:55
 */
public interface TravelItemDao {
    public void addItem(TravelItem travelItem);

    Page<TravelItem> findPage(String queryString);

    TravelItem find(Integer id);

    void edit(TravelItem travelItem);

    int countTravelItemIds(Integer id);

    void delete(Integer id);

    List<TravelItem> findAll();

    List<TravelItem> findTravelItemListById(Integer id);
}
