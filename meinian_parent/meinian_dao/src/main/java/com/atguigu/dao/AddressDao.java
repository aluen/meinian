package com.atguigu.dao;

import com.atguigu.pojo.Address;
import com.github.pagehelper.Page;

import java.util.List;

/**
 * @author captainlhy
 * @create 2021-06-08 20:46
 */
public interface AddressDao {
    List<Address> findAll();

    Page<Address> findPage(String queryString);
}
