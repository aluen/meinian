package com.atguigu.dao;

import com.atguigu.pojo.Role;

import java.util.Set;

/**
 * @author captainlhy
 * @create 2021-06-07 21:10
 */
public interface RoleDao {
    Set<Role> findRolesByUserId(Integer userId);

}
