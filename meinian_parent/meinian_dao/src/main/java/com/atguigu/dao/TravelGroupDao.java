package com.atguigu.dao;

import com.atguigu.pojo.TravelGroup;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * @author captainlhy
 * @create 2021-06-01 19:31
 */
public interface TravelGroupDao {
    void add(TravelGroup travelGroup);

    void setTravelGroupAndTravelItem(Map param);

    Page<TravelGroup> findPage(String queryString);

    TravelGroup get(Integer id);

    List<Integer> getTravelItemIdsByTravelGroupId(Integer travelGroupId);

    void deleteTravelItemIdsByTravelGroupId(Integer id);

    void edit(TravelGroup travelGroup);

    int countTravelGroupIds(Integer id);

    void delete(Integer id);

    List<TravelGroup> findAll();

    List<TravelGroup> findTravelGroupListById(Integer id);
}
