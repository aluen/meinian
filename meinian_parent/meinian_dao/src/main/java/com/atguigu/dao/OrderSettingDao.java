package com.atguigu.dao;

import com.atguigu.pojo.OrderSetting;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author captainlhy
 * @create 2021-06-04 13:53
 */
public interface OrderSettingDao {
    void add(OrderSetting orderSetting);

    int findCountByOrderDate(Date orderDate);

    void editNumberByOrderDate(int number);

    List<OrderSetting> getOrderSettingByMonth(Map map);


    void edit(OrderSetting orderSetting);

    OrderSetting getOrderSettingByOrderDate(Date orderDate);

    void editReservationsByOrderDate(OrderSetting orderSetting);
}
