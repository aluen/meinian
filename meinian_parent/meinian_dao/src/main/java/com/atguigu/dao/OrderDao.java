package com.atguigu.dao;

import com.atguigu.pojo.Order;

import java.util.List;
import java.util.Map;

/**
 * @author captainlhy
 * @create 2021-06-07 18:40
 */
public interface OrderDao {
    void add(Order order);

    int getCountByCondition(Order order);

    Map findById(Integer orderId);

    int getTodayOrderNumber(String date);

    int getTodayVisitsNumber(String date);

    int getThisWeekAndMonthOrderNumber(Map<String, Object> map);

    int getThisWeekAndMonthVisitsNumber(Map<String, Object> map);

    List<Map<String, Object>> findHotSetmeal();

}
