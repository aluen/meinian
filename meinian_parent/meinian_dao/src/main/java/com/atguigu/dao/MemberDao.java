package com.atguigu.dao;

import com.atguigu.pojo.Member;

/**
 * @author captainlhy
 * @create 2021-06-07 18:39
 */
public interface MemberDao {
    Member getMemberByTelephone(String telephone);

    void add(Member member);

    Member findByTelephone(String telephone);

    int getMemberCountByMonth(String lastDayOfMonth);

    public Integer findMemberCountBeforeDate(String date);

    int getTodayNewMember(String date);

    int getTotalMember();

    int getThisWeekAndMonthNewMember(String date);

}
