package com.atguigu.dao;

import com.atguigu.pojo.Setmeal;
import com.github.pagehelper.Page;

import java.util.List;
import java.util.Map;

/**
 * @author captainlhy
 * @create 2021-06-02 18:04
 */
public interface SetmealDao {
    void add(Setmeal setmeal);

    void setSetmealAndTravelGroup(Map<String, Integer> map);

    Page<Setmeal> findPage(String queryString);

    List<Integer> getTravelGroupIdsBySetmealId(Integer setmealId);

    Setmeal get(Integer id);

    int countTravelGroupIds(Integer id);

    void delete(Integer id);

    void deleteMidData(Integer id);

    void edit(Setmeal setmeal);

    List<Setmeal> findAll();

    Setmeal findById(Integer id);

    List<Map<String, Object>> getSetmealReport();
}
