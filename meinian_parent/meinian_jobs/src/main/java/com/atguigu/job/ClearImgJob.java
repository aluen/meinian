package com.atguigu.job;

import com.atguigu.constant.RedisConstant;
import com.atguigu.util.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.JedisPool;

import java.util.Set;

//执行清理垃圾图片的任务类
public class ClearImgJob {

    @Autowired
    JedisPool jedisPool;

    public void clearImg(){
        //求差集： 获取第一个集合中的元素 -> 第一个集合中元素在第二个集合中不存在的元素。
        Set<String> sdiff = jedisPool.getResource()
                .sdiff(RedisConstant.SETMEAL_PIC_RESOURCES, RedisConstant.SETMEAL_PIC_DB_RESOURCES);
        if (sdiff!=null && sdiff.size()>0){
            for (String imgName : sdiff) {
                QiniuUtils.deleteFileFromQiniu(imgName);
                jedisPool.getResource().srem(RedisConstant.SETMEAL_PIC_RESOURCES,imgName);
            }
        }
    }
}
