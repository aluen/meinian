package com.atguigu.service;

import com.alibaba.dubbo.config.annotation.Service;
import com.atguigu.entity.PageResult;
import com.atguigu.pojo.TravelItem;

import java.util.List;

/**
 * @author captainlhy
 * @create 2021-05-31 18:47
 */

public interface TravelItemService {

    public void addItem(TravelItem travelItem);

    PageResult findPage(Integer currentPage, Integer pageSize, String queryString);

    TravelItem find(Integer id);

    void edit(TravelItem travelItem);

    void delete(Integer id);

    List<TravelItem> findAll();
}
