package com.atguigu.service;

import com.atguigu.entity.PageResult;
import com.atguigu.pojo.Setmeal;

import java.util.List;
import java.util.Map;

/**
 * @author captainlhy
 * @create 2021-06-02 18:02
 */
public interface SetmealService {
    void add(Integer[] travelgroupIds, Setmeal setmeal);

    PageResult findPage(Integer currentPage, Integer pageSize, String queryString);

    List<Integer> getTravelGroupIdsBySetmealId(Integer setmealId);

    Setmeal get(Integer id);

    void delete(Integer id);

    void edit(Integer[] travelgroupIds, Setmeal setmeal);

    List<Setmeal> findAll();

    Setmeal findById(Integer id);

    List<Map<String, Object>> getSetmealReport();
}
