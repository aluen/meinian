package com.atguigu.service;

import com.atguigu.pojo.OrderSetting;

import java.util.List;
import java.util.Map;

/**
 * @author captainlhy
 * @create 2021-06-04 13:52
 */
public interface OrderSettingService {
    void add(List<OrderSetting> orderSettings);

    List<Map<String, Integer>> getOrderSettingByMonth(String date);

    void edit(OrderSetting orderSetting);
}
