package com.atguigu.service;

import com.atguigu.pojo.User;

/**
 * @author captainlhy
 * @create 2021-06-07 20:54
 */
public interface UserService {
    User findUserByUsername(String username);
}
