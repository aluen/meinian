package com.atguigu.service;

import java.util.Map;

/**
 * @author captainlhy
 * @create 2021-06-08 19:26
 */
public interface ReportService {
    Map<String, Object> getBusinessReportData();
}
