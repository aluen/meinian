package com.atguigu.service;

import com.atguigu.entity.PageResult;
import com.atguigu.pojo.Address;

import java.util.List;

/**
 * @author captainlhy
 * @create 2021-06-08 20:47
 */
public interface AddressService {
    List<Address> findAll();

    PageResult findPage(Integer currentPage, Integer pageSize, String queryString);
}
