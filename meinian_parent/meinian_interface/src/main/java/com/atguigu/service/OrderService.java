package com.atguigu.service;

import com.atguigu.entity.Result;

import java.util.Map;

/**
 * @author captainlhy
 * @create 2021-06-07 14:05
 */
public interface OrderService {
    Result save(Map map) throws Exception;

    Map findById(Integer orderId) throws Exception;
}
